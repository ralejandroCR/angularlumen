<?php


namespace App\Http\Controllers;
use App\Http\Models\Persona;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Exception;



class PersonaController
{
    public function __construct()
    {

    }

    public function index(){
        try{
            $listado = Persona::all();
            return response()->json($listado, Response::HTTP_OK);
        }catch(Exception $ex){
            return response()->json([
                'error' => 'Hubo un error al listar la informacion: '. $ex->getMessage()
            ], 206);
        }
    }

    /*
     * Retorna persona con el id pasado por paramtro
     * */
    public function mostrar(Request $request, $id){
        try{
            $person = Persona::find($id);
            return response()->json($person, Response::HTTP_OK);
        } catch (Exception $ex){
            return response()->json([
                'error' => 'Hubo un error al buscar a la perosna con id: '. $id. $ex->getMessage()
            ], 404);
        }
    }

    public function guardar(Request $request){
        try{
            $nombre= $request->input('nombre');
            $apellido = $request->input('apellido');
            $apodo = $request->input('apodo');
            /*$save = Persona::create([
                'nombre'=> $nombre,
                'apellido'=> $apellido,
                'apodo'=> $apodo
            ]);*/
            //$res['success'] = true;
             //return response($res, 200);
            $person = Persona::create($request->all());
            return response()->json($person, Response::HTTP_CREATED);
        }catch(Exception $ex){
            return response()->json([
                'error' => 'Hubo un error al realizar el registro sd: '.$ex->getMessage()
            ], 400);
        }
    }

    public function modificar(Request $request, $id){
        try{
            $person = Persona::findOrFail($id);
            $person -> update($request->all());
            return response()->json($person, Response::HTTP_OK);
        }catch(Exception $ex){
            return response()->json([
                'error' => 'Hubo un error al modificar el registro con id: '.$id. ' : '.$ex->getMessage()
            ], 400);
        }
    }

    public function eliminar(Request $request, $id){
        try{
            Persona::find($id)->delete();
            return response()->json([], Response::HTTP_OK);
        }catch(Exception $ex){
            return response()->json([
                'error' => 'Hubo un error al eliminar el registro con id: '.$id. ' : '.$ex->getMessage()
            ], 400);
        }
    }

}
