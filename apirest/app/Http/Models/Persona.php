<?php


namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table='persona';
    protected $primaryKey='id';
    protected $fillable=['id','nombre','apellido','apodo'];
}
