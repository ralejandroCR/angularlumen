<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'app'], function() use($router){
    $router->get('/persona', 'PersonaController@index');
    $router->post('/persona', 'PersonaController@guardar');
    $router->get('/persona/{id}', 'PersonaController@mostrar');
    $router->put('/persona/{id}', 'PersonaController@modificar');
    $router->delete('/persona/{id}', 'PersonaController@eliminar');
});
