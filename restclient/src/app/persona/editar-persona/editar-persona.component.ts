import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from 'src/app/models/persona';
import { PersonaService } from 'src/app/servicio/persona.service';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {first} from 'rxjs/operators';


@Component({
  selector: 'app-editar-persona',
  templateUrl: './editar-persona.component.html',
  styleUrls: ['./editar-persona.component.css']
})
export class EditarPersonaComponent implements OnInit {
  personaForm = new FormGroup({
    id: new FormControl(''),
    nombre: new FormControl(''),
    apellido: new FormControl(''),
    apodo: new FormControl(''),
    created_at: new FormControl(''),
    updated_at: new FormControl('')
  });
  persona: Persona;
  constructor(private formBuilder: FormBuilder, private ruta: Router, private servicioPersona: PersonaService) { }

  ngOnInit() {
    const personaId = localStorage.getItem('editarId');
    this.servicioPersona.getPersona(+ personaId).subscribe( data => {
      this.personaForm.setValue(data);
    });
  }

  onSubmit() {
    this.servicioPersona.modificarPersona(this.personaForm.value)
      .pipe(first())
      .subscribe( data => {
        this.ruta.navigate(['lista-persona']);
      }, error => {
        alert('Hubo un error al modificar el cargo:' + error.message);
      });
  }

  volverListado(): void {
    this.ruta.navigate(['lista-persona']);
  }

}
