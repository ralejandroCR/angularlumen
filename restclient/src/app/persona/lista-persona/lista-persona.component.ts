import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Persona } from 'src/app/models/persona';
import { PersonaService } from 'src/app/servicio/persona.service';

@Component({
  selector: 'app-lista-persona',
  templateUrl: './lista-persona.component.html',
  styleUrls: ['./lista-persona.component.css']
})
export class ListaPersonaComponent implements OnInit {
  listapersonas =  new Array<Persona>();
  nombre = '';
  constructor(private ruta: Router, private servicioPersona: PersonaService) {}

  ngOnInit() {
    this.listarPersonas();
  }

  listarPersonas() {
    this.servicioPersona.getListadoPersonas().subscribe(
      data => {
        this.listapersonas = data;
        console.log('listado de personas: ' + this.listapersonas);
      }
    );
  }

  agregarPersona(): void {
    this.ruta.navigate(['agregar-persona']);
  }
  eliminarPersona( persona: Persona): void {
    this.servicioPersona.eliminarPersona(persona.id).subscribe(
      data => {
        this.listarPersonas();
      }, (err) => {
        console.log('Hubo un error al Eliminar el Cargo => ' + err.toString());
      });

    window.alert('Se ha eliminado');
  }

  editarPersona(person: Persona): void {
    localStorage.removeItem('editarId');
    localStorage.setItem('editarId', person.id.toString());
    this.ruta.navigate(['editar-persona']);
  }
}
