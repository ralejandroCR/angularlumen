import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { PersonaService } from 'src/app/servicio/persona.service';

@Component({
  selector: 'app-agregar-persona',
  templateUrl: './agregar-persona.component.html',
  styleUrls: ['./agregar-persona.component.css']
})
export class AgregarPersonaComponent implements OnInit {
  personaForm = new FormGroup({
    nombre: new FormControl(''),
    apellido: new FormControl(''),
    apodo: new FormControl(''),
  });
  constructor(private formBuilder: FormBuilder, private ruta: Router, private servicioPersona: PersonaService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.servicioPersona.agregarPersona(this.personaForm.value)
      .subscribe(data => {
        this.ruta.navigate(['lista-persona']);
      });
  }

  volverListado(): void {
    this.ruta.navigate(['lista-persona']);
  }

}
