import { Injectable } from '@angular/core';
import { Persona } from './../models/persona';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  // Direccion API
  private API_REST = 'http://127.0.0.1:80/apirest/public/app/persona';
  private httpHeader = new HttpHeaders( {
    'Content-Type' : 'application/json',
    'Authorization': 'RcgkvUAAOpGckyWonLANuTAZEFtU7VkZ'
  });

  constructor(private http: HttpClient) {
    console.log('Servicio is Running...!');
  }
  // Obtener todos las personas
  getListadoPersonas(): Observable<any[]> {
    return this.http.get<Persona[]>(this.API_REST);
  }
  // obtener segun id
  getPersona(id: number): Observable <any> {
    return this.http.get<Persona>(this.API_REST + '/' + id);
  }
  // Crea un nuevo registroo
  agregarPersona(nuevaPersona: Persona): Observable<any> {
    return this.http.post<Persona>( this.API_REST, nuevaPersona , { headers: this.httpHeader});
  }
  // Actualiza
  modificarPersona(person: Persona): Observable<any> {
    return this.http.put<Persona>( this.API_REST + '/' + person.id, person , { headers: this.httpHeader});
  }
  // Elimina una persona segun id
  eliminarPersona(id: number): Observable<any> {
    return this.http.delete<Persona>( this.API_REST + '/' + id, { headers: this.httpHeader});
  }
}
