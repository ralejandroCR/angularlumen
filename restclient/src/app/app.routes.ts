import { Routes} from '@angular/router';
import { ListaPersonaComponent } from './persona/lista-persona/lista-persona.component';
import { AgregarPersonaComponent } from './persona/agregar-persona/agregar-persona.component';
import { EditarPersonaComponent } from './persona/editar-persona/editar-persona.component';


export const routes: Routes = [
  { path: 'lista-persona', component: ListaPersonaComponent},
  { path: 'agregar-persona', component: AgregarPersonaComponent},
  { path: 'editar-persona', component: EditarPersonaComponent},
  { path: '**', redirectTo: 'lista-persona' }
];

/*export const routes: Routes = [{
  path: 'lista-persona',
  component: ListaPersonaComponent,
  children: [{
    path: 'agregar-persona',
    component: AgregarPersonaComponent
  }, {
    path: 'editar-persona',
    component: EditarPersonaComponent,
  }]
}, {
  path: '**',
  redirectTo: 'error/404'
}];*/
